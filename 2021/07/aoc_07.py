


def how_much_is_the_fuel(crablist, position: int) -> int:
    
    min_fuel = 0
    
    for crab in crablist:
        min_fuel += abs(crab - position)
    
    return min_fuel

def how_much_is_the_crab_engine(crablist, position: int) -> int:
    
    min_fuel = 0
    
    for crab in crablist:
        n = abs(crab - position)
        min_fuel += n*(n+1)/2
    
    return min_fuel


def main():

    f = open("input", "r")

    data = f.readlines()

    data = [int(x) for x in data[0].rstrip().split(",")]
    
    fuels = []
    for i in range(max(data)):
        fuels.append(how_much_is_the_fuel(data, i))
    print("Aufgabe 1:", min(fuels))

    fuels = []
    for i in range(max(data)):
        fuels.append(how_much_is_the_crab_engine(data, i))
    print("Aufgabe 2:", min(fuels))


if __name__ == "__main__":
    main()
