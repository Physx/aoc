# aoc 02

def get_aim_position(sonar):
    
    h = 0
    v = 0
    aim = 0

    for i in range(len(sonar)):
        print(sonar[i])
        value = sonar[i][1]
        if sonar[i][0] == "up":
            aim -= value
        elif sonar[i][0] == "down":
            aim += value
        else:
            h += value
            v += aim * value
        print(h,v,aim)

    return h*v

def get_position(sonar):
    
    h = 0
    v = 0

    for i in range(len(sonar)):
        value = sonar[i][1]
        if sonar[i][0] == "up":
            v -= value
        elif sonar[i][0] == "down":
            v += value
        else:
            h += value

    return h * v


def main():

    f = open("input", "r")
    
    sonar = []

    for line in f.readlines():
        split = line.split(" ")
        pair = (split[0], int(split[1]))
        sonar.append(pair)
    
    print(get_aim_position(sonar))

if __name__ == "__main__":
    main()
