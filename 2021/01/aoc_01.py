# aoc 01

def compare_each(sonar):

    counter = 0
    for i in range(len(sonar) - 1):
        if sonar[i+1] > sonar[i]:
            count += 1

    return counter

def compare_blockwise(sonar):
    
    counter = 0

    for i in range(len(sonar) - 3):
        a = sum(sonar[i:i+3])
        b = sum(sonar[i+1:i+4])
        if b > a:
            counter += 1

    return counter


def main():

    f = open("input", "r")
    
    sonar = []
    for line in f.readlines():
        sonar.append(int(line))

    print(compare_each(sonar))
    print(compare_blockwise(sonar))


if __name__ == "__main__":
    main()
