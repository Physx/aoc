# aoc 03

import numpy as np

def bit_to_dec(bitlist):
    out = 0 
    for bit in bitlist:
        out = (out << 1) | bit
    return out

def power_consumption(m):
    
    matrix = np.array(m)
    
    m = matrix.shape[0]
    n = matrix.shape[1]

    gamma = np.zeros((n), dtype=int)
    eps = np.zeros((n), dtype=int)
    
    summy = np.sum(matrix, axis=0)

    for i, elem in enumerate(summy):
        if elem >= m/2:
            gamma[i] = 1
            eps[i] = 0
        else:
            gamma[i] = 0
            eps[i] = 1
    
    return gamma, eps

def rating(m, param, gamma=True) -> list:
    # print("parameter:", param)
    matrix = np.array(m)
    m = matrix.shape[0]
    n = matrix.shape[1]

    rate = []
    
    for k in range(n):
        # iterate elementwise through array
        for j in range(len(matrix)):
            if matrix[j][k] == param[k]:
                rate.append(matrix[j])
        # print("k:", k, oxygen)
        # print("len oxy:", len(oxygen))
        if len(rate) == 1:
            break
        if gamma == True:
            param, _ = power_consumption(rate)
        else:
            _, param = power_consumption(rate)
        matrix = rate
        oxygen = []

    return rate[0]


def main():

    f = open("input", "r")
    
    matrix = []

    for line in f.readlines():
        
        nums = [int(x) for x in line if x != '\n'] # get rid of next line
        matrix.append(nums)

    gamma, eps = power_consumption(matrix)

    oxy  = bit_to_dec(rating(matrix, gamma))
    co2 = bit_to_dec(rating(matrix, eps, False))
    
    print("oxy x co2 =", oxy*co2)


    

if __name__ == "__main__":
    main()
