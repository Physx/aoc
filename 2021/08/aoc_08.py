


def compare(one, two):
    
    return sorted(one) == sorted(two)
    

def decipher(arr, d):

    erg = ""

    for elem in arr:
        for key in d:
            if compare(elem, d[key]):
                erg += str(key)
    return int(erg)

def make_dict(arr):

    d = {}
    len_5 = []
    len_6 = []

    for elem in arr:
        if len(elem) == 2:
            d[1] = elem
        elif len(elem) == 3:
            d[7] = elem
        elif len(elem) == 4:
            d[4] = elem
        elif len(elem) == 5:
            len_5.append({x for x in elem})
        elif len(elem) == 6:
            len_6.append({x for x in elem})
        elif len(elem) == 7:
            d[8] = elem
    
    set_0 = set()
    set_1 = {x for x in d[1]}
    set_2 = set()
    set_3 = set()
    set_4 = {x for x in d[4]}
    set_5 = set()
    set_6 = set()
    set_7 = {x for x in d[7]}
    set_8 = {x for x in d[8]}
    set_9 = set()

    for char in set_1:
        set_0.add(char)
        set_3.add(char)
        set_9.add(char)

    for char in set_4.difference(set_1):
        set_5.add(char)
        set_6.add(char)
        set_9.add(char)

    for char in set_8.difference(set_4).difference(set_7):
        set_0.add(char)
        set_2.add(char)
        set_6.add(char)


    top_char = set_7.difference(set_1)
    top_char = top_char.pop()
    set_0.add(top_char)
    set_2.add(top_char)
    set_3.add(top_char)
    set_5.add(top_char)
    set_6.add(top_char)
    set_9.add(top_char)
    
    for s in len_5:
        if len(set_2.difference(s)) == 0:
            set_2 = s
        elif len(set_3.difference(s)) == 0:
            set_3 = s
        else:
            set_5 = s

    for s in len_6:
        if len(set_0.difference(s)) == 0:
            set_0 = s
        elif len(set_9.difference(s)) == 0:
            set_9 = s
        else:
            set_6 = s

    list_of_set = [set_0, set_1, set_2, set_3, set_4, set_5, set_6, set_7, set_8, set_9]
    
    for i, elem in enumerate(list_of_set):
        empty = ""
        for char in elem:
            empty += char
        d[i] = empty

    return d


def main():

    f = open("input", "r")

    data = f.readlines()

    # data = [int(x) for x in data[0].rstrip().split(",")]

    unique_len = {2, 3, 4, 7}
    counter = 0


    print("Aufgabe 1")
    for line in data:
        code = line.rstrip().split("|")[1].split()
        for each in code:
            if len(each) in unique_len:
                counter += 1
    print("Lsg:", counter)

    print("Aufgabe 2")
    counter = 0
    for line in data:
        wusel   = line.rstrip().split("|")[0].split()
        code    = line.rstrip().split("|")[1].split()
        counter += decipher(code, make_dict(wusel))

    print("Lsg:", counter)

if __name__ == "__main__":
    main()
