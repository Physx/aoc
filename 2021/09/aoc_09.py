

import numpy as np

class neighbor:

    def __init__(self, arr):

        self.left = arr[0]
        right
        top
        down
        


def get_basin(position, cave):

    for neighbour in position.neighbours:
        if neighbour.value > 8:
            return 1
        else:
            get_basin(neighbour, cave)

    
def smallest(number, arr):

    if number < min(arr):
        return True
    else:
        False

def find_minimum_height(arr):

    risk = []

    arr_height = arr.shape[0]
    arr_width = arr.shape[1]
    
    for i in range(arr_height):
        for j in range(arr_width):
            center = arr[i,j]
            #edges
            if i == 0:
                left = 10
            else:
                left = arr[i-1, j]

            if i == arr_height - 1: #right side
                right = 10
            else:
                right = arr[i+1,j]

            if j == 0: #up
                top = 10
            else:
                top = arr[i, j-1]

            if j == arr_width - 1: #down
                down = 10
            else:
                down = arr[i, j+1]

            neighbors = [top, left, right, down]

            if smallest(center, neighbors):
                risk.append((center, (i,j)))
                print(np.gradient(arr, edge_order=1))


    return np.array(risk)


def main():

    f = open("test", "r")

    data = f.readlines()
    
    matrix = []
    
    

    print("Aufgabe 1")
    for row in data:
        matrix.append([int(x) for x in row.rstrip()])
    m = np.array(matrix)
    print(m)


    risk_levels = find_minimum_height(m)

    print(risk_levels)


if __name__ == "__main__":
    main()
