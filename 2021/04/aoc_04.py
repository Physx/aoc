# aoc 03

import numpy as np

class board():

    def __init__(self, matrix):

        self.grid = 5
        self.m = np.zeros((self.grid, self.grid), dtype=object)

        for i in range(self.grid):
            for j in range(self.grid):
                self.m[i][j] = [False, matrix[i][j]]

    def mark(self, number):
        # check number in board

        for i in range(self.grid):
            for j in range(self.grid):
                if self.m[i][j][1] == number:
                    self.m[i][j][0] = True

    def bingo(self) -> bool:
        
        status = True
        for row in self.m:
            status = True
            for elem in row:
                if elem[0] is False:
                    status = False
                    break
            if status is True:
                return status


        new_m = self.m.T
        for row in new_m:
            status = True
            for elem in row:
                if elem[0] is False:
                    status = False
                    break
            if status is True:
                return status
        return status



    def __str__(self):
        return str(self.m)

    
    def calc(self, random):
        
        summy = 0
        
        for i in range(self.grid):
            for j in range(self.grid):
                if self.m[i][j][0] is False:
                    summy += self.m[i][j][1]

        return summy*random
                    


        

def main():

    f = open("input", "r")
    
    data = f.readlines()

    board_list = []
    random_list = np.array(data[0].split(","), int)

    boards = []

    data = data[1:]
    
    for i, line in enumerate(data):
        if not line.strip():
            matrix = []
            for j in range(5):
                matrix.append(data[i+j+1].strip().replace("  ", " ").split(" "))
            
            i += 5
            board_list.append(matrix)
            # matrix.append(board_list)

    
    board_list = np.array(board_list, int)

    
    for b in board_list:
        boards.append(board(b))
    
    for i, each_number in enumerate(random_list):
        if len(boards) == 1:
            last_board = boards[0]
            break
        print(each_number)
        for b in boards:
            b.mark(each_number)
            if b.bingo():
                print("BINGO")
                print(b)
                boards.remove(b)

                # print(b.calc(each_number))
    for i, each in enumerate(random_list):
        print(each)
        last_board.mark(each)
        if last_board.bingo():
            print(last_board.calc(random_list[i+1]))
            break
                

if __name__ == "__main__":
    
    test_matrix = np.array([
        [22, 13, 17, 11, 0],
        [8, 2, 23,  4, 24],
        [21,  9, 14, 16,  7],
        [6, 10,  3, 18,  5],
        [1, 12, 20, 15, 19]
        ])

    test = board(test_matrix)
    
    test.mark(13)
    test.mark(2)
    test.mark(9)
    test.mark(10)
    test.mark(12)
    
    main()
