# aoc 01

import numpy as np
from time import sleep


def check_next_elem(arr, pos):
    
    counter = 0
    
    for i in range(pos, len(arr)):
        print(i)



    return counter

def move_east(m):
    
    x = m.shape[0]
    y = m.shape[1]

    east = '>'
    dot  = '.'

    i,j = 0,0
    for i in range(x):
        j = 0
        # print(i,j,m[i][j])
        while j < y:
            if m[i][j] == east:
                if j < y-1:
                    # print(i, j, m[i][j], "move")
                    if m[i][j+1] == dot:
                        m[i][j], m[i][j+1] = dot, east
                        j += 1
                elif j == y-1 and m[i][0] == dot:
                    m[i][0], m[i][y-1] = east, dot
            j += 1

    return m

def replace(m, before, after):

    x = m.shape[0]
    y = m.shape[1]

    matrix = []

    for i in range(x):
        l = []
        for j in range(y):
            if m[i][j] == before:
                l.append(after)
            elif m[i][j] == after:
                l.append(before)
            else:
                l.append('.')
        matrix.append(l)

    return np.array(matrix)

def move_south(m):
    
    
    east  = '>'
    south = 'v'

    m = replace(m, south, east)
    # print("replace")
    # print(m)
    transpose_m = m.T
    # print("transpose")
    # print(transpose_m)
    move_e = move_east(transpose_m)
    # print("move_e")
    # print(move_e)
    trans_e = move_e.T
    return replace(trans_e, '>', 'v')



def main():

    f = open("test", "r")
    
    data = f.readlines()
    matrix = []

    for line in data:
        l = [str(x) for x in line if x != '\n']
        matrix.append(l)
    
    matrix = np.array(matrix, str)
    print(matrix)
    print()
    counter = 1
    while True:
        print(counter)
        matrix = move_east(matrix)
        matrix = move_south(matrix)

        # temp = move_east(matrix)
        # temp = move_south(matrix)

        # if np.all(matrix == temp):
            # break
        print(matrix)
        sleep(2)
        counter += 1
    print(counter)
    # print(move_east(matrix))
    # move_south(matrix)
    # print(move_south(matrix))



if __name__ == "__main__":
    main()

