from utils import read

class Game():
    def __init__(self, array):
        list_split = array.split(":")
        self.id = int(list_split[0].split()[1])
        fields = list_split[1].strip().split("|")

        self.winning = [int(i) for i in fields[0].strip().split()]
        self.your_numbers = [int(i) for i in fields[1].strip().split()]

        self.points = 0

        self.play()

    def play(self):
        for number in self.your_numbers:
            if number in self.winning:
                self.points += 1

def main(input):
    text = read(input)

    games = []
    for game in text:
        games.append(Game(game))

    dict_games = {}
    res = [dict_games.update({i.id: 1}) for i in games]
    dict_games[games[0].id] = 1

    for game in games:
        for k in range(game.id + 1, game.id + game.points + 1):
            dict_games[k] += dict_games[game.id]


    
    print(dict_games)

    print(sum(dict_games.values()))

if __name__ == "__main__":
    main("input")