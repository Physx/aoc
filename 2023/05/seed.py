from utils import read
import numpy as np
from numba import njit, prange, int64
from numba.experimental import jitclass

spec = [
    ("map", int64[:]),
    ("seed", int64)
]

@jitclass(spec)
class Map:
    def __init__(self, list):
        # self.name = list.pop(0).split("-")[2].split()[0]
        self.map = np.array([])
        for line in list:
            np.append(self.map, [int64(i) for i in line.split()])
        self.seed = 0
        
    def set_seed(self, seed):
        self.seed = seed
            
    def get_location(self):
        result = []
        for liste in self.map:
            if self.seed >= liste[1] and self.seed < liste[1] + liste[2]:
                temp =  self.seed + (liste[0] - liste[1])
                result.append(temp)
                    
        if not result:
            return self.seed
        return min(result)
        


def main(input_file):
    text = read(input_file)
    
    seeds = [int(i) for i in text.pop(0).split(":")[1].split()]
    
    seeds_b = []
    
    for i in range(0, len(seeds), 2):
        seeds_b.append(np.arange(seeds[i], seeds[i] + seeds[i+1], 1))
    
    print(seeds_b)
    
    # breakpoint()
    # remove first element
    text.pop(0)
    
    maps = []
    map_list = []
    
    for line in text:
        if line:
            print(line)
            line.pop(0)
            map = np.array([])
            for l in line:
                np.append(map, [int64(i) for i in l.split()])
                print(map)
            
            map_list.append(line)
        else:
            maps.append(Map(map_list))
            map_list = []
    # last element
    maps.append(Map(map_list))
    
    low = seek_lowest(seeds_b, maps)
    
    print(low)

@njit(parallel=True)
def seek_lowest(seeds, maps):
    lowest = []
    for i in prange(len(seeds)):
        for j in prange(len(seeds[i])):
            seed = seeds[i][j]
            for k in prange(len(maps)):
                maps[k].set_seed(seed)
                new_seed = maps[k].get_location()
                # print(m.name)
                # print(new_seed)
                seed = new_seed
                if k == 6:
                    lowest.append(new_seed)
    return min(lowest)

    
if __name__ == "__main__":
    main("test")
    
    # print(20358599)