
import collections


def main():
    
    f = open("input", "r")
    
    each_line = []
    
    for line in f.readlines():
        each_line.append(line.strip())
    
    numbers = [None] * len(each_line)
    for i, word in enumerate(each_line):
        numbers[i] = []
        for val in word:
            try:
                int(val)
                numbers[i].append(val)
            except:
                pass
            
    print(add(numbers))
    
def add(numbers):
    sum = 0
    for line in numbers:
        if len(line) == 1:
            sum += int(str(line[0]) + str(line[0]))
        else:
            sum += int(str(line[0]) + str(line[-1]))
            
    return sum


def main2():
    
    f = open("input", "r")
    
    each_line = []
    
    for line in f.readlines():
        each_line.append(line.strip())
                
    
    numbers = [None] * len(each_line)

    for i, word in enumerate(each_line):
        numbers[i] = find(word)
    
    print(add(numbers))
    
    

def find(word):
    real_words = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
                  "0","1","2","3","4","5","6","7","8","9"]
    dict_of_numbers = {
        "one": 1,
        "two": 2,
        "three": 3,
        "four": 4,
        "five": 5,
        "six": 6,
        "seven": 7,
        "eight": 8,
        "nine": 9
    }
    result = {}
    end = []
    for number in real_words:
        if word.find(number) != -1:
            pos0 = word.find(number)
            try:
                number = dict_of_numbers[number]
            except:
                pass
            result.update({pos0: number})
        
            
    for number in real_words:
        if word.rfind(number) != -1:
            pos0 = word.rfind(number)
            try:
                number = dict_of_numbers[number]
            except:
                pass
            result.update({pos0: number})
            
    od = collections.OrderedDict(sorted(result.items()))
    
    for k, v in od.items():
        end.append(v)
        
    return end
        
        
if __name__ == "__main__":
    
    main2()
    