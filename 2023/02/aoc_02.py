
class Game():
    def __init__(self, line) -> None:
        self.id = int(line.split(":")[0].split(" ")[1])
        self.set = []
        for i in line.split(":")[1].split(";"):
            self.set.append({})
        self.red = 0
        self.green = 0
        self.blue = 0

        for i, l in enumerate(line.split(":")[1].split(";")):
            for k in l.strip().split(","):
                number = int(k.strip().split(" ")[0])
                color = k.strip().split(" ")[1]
                if color == "red":
                    if number > self.red:
                        self.red = number
                if color == "green":
                    if number > self.green:
                        self.green = number
                if color == "blue":
                    if number > self.blue:
                        self.blue = number
                self.set[i].update({color: number})

        


    def is_game_possible(self, r, g, b):
        for s in self.set:
            if "red" in s:
                if s["red"] > r:
                    return False
            if "green" in s:
                if s["green"] > g:
                    return False
            if "blue" in s:
                if s["blue"] > b:
                    return False
        
        return True
        

def read(input):
    
    f = open(input, "r")
    
    each_line = []
    
    for line in f.readlines():
        each_line.append(line.strip())
    
    f.close()
    return each_line

def main():
    text = read("input")
    games = []
    result = 0
    for line in text:
        games.append(Game(line))

    for g in games:
        result += g.red * g.green * g.blue
        # if g.is_game_possible(12, 13, 14):
        #     result += g.id
    
    print(result)


if __name__ == "__main__":
    main()