from utils import read
import numpy as np

# from high to low
cards = ["A", "K", "Q", "J", "T", "9", "8", "7", "6", "5", "4", "3", "2"]
poker_type = {
    "Five of a kind": 6,
    "Four of a kind": 5,
    "Full House": 4,
    "Three of a kind": 3,
    "Two pair": 2,
    "One pair": 1,
    "High card": 0
}

def custom_sort(card):
    # Extrahiere die Werte der Karten aus dem String
    values = card.get_input()
    
    # Vergleiche die Positionen der Werte in der Kartenliste
    sorted_values = sorted(values, key=lambda x: cards.index(x))
    print(sorted_values)
    
    # Verbinde die sortierten Werte zu einem String und gib ihn zurück
    return ''.join(sorted_values)

class Hand:
    def __init__(self, cards):
        
        self.poker = {"A":0, "K":0, "Q":0, "J":0, "T":0, "9":0, "8":0, "7":0, "6":0, "5":0, "4":0, "3":0, "2":0}
        
        self.bid = cards.split()[1]
        cards = cards.split()[0]
        self.input = cards
        
        for card in cards:
            self.poker[card] += 1
            
        self.sorted_hand = [v for v in sorted(self.poker.values(), reverse=True)]
        # self.rank = self.get_rank()
    def get_bid(self):
        return self.bid
    def get_input(self):
        return self.input
    
    def get_poker_hand(self):
        return self.poker
        
    def get_rank(self):
        first_value = self.sorted_hand[0]
        scnd_value = self.sorted_hand[1]
        if first_value == 5:
            return poker_type["Five of a kind"]
        if first_value == 4:
            return poker_type["Four of a kind"]
        if first_value == 3 and scnd_value == 2:
            return poker_type["Full House"]
        if first_value == 3:
            return poker_type["Three of a kind"]
        if first_value == 2 and scnd_value == 2:
            return poker_type["Two pair"]
        if first_value == 2:
            return poker_type["One pair"]
        if first_value == 1:
            return poker_type["High card"]
        
            
        
        

def main(file):
    text = read(file)
    
    hands = []

    for hand in text:
        hands.append(Hand(hand))
        
    sorted_hands = [h for h in sorted(hands, key=lambda x: x.get_rank())]
    hand_dict = {1:[], 2:[], 3:[], 4:[], 5:[], 6:[], 7:[]}
    
    for hand in sorted_hands:
        hand_dict[hand.get_rank()].append([hand.get_input(), hand.get_bid()])
    
    
    result = []
    for key, hand in hand_dict.items():
        if len(hand) == 1:
            result.append(hand[0])
        else:
            print(hand)
            sorted_list = sorted([h[0] for h in hand], key=lambda x: [cards.index(c) for c in x], reverse=True)
            print(sorted_list)
            for h in sorted_list:
                result.append(h)
        
    summie = 0
    

if __name__ =="__main__":
    main("test")