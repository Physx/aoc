from utils import read
import numpy as np

from numba import njit, prange

# @njit(parallel=True, cache=True)
def winning(stop, dest):
    stoptime = stop
    destination = dest
    tries = 0
    internal_status = False
    status = False
    for i in range(stoptime):
        distance = i * (stoptime - i)
        if distance > destination:
            tries += 1
            internal_status = True
        if internal_status and distance < destination:
            status = True
        if status:
            break
            
    return tries

def main2(file):
    text = read(file)
    time = text[0].split(":")[1].split()
    dest = text[1].split(":")[1].split()
    racing_time = ""
    destination_time = ""
    for i in range(len(time)):
        racing_time += time[i]
        destination_time += dest[i]
    
    print(racing_time, destination_time)
    result = winning(int(racing_time), int(destination_time))
    
    print(result)
    
    

def main(file):
    text = read(file)
    
    # speed = m/s
    race_conditions = np.zeros((len(text[0].split(":")[1].split()), 2), int)
    
    for i, times in enumerate(text):
        line = times.split(":")[1].split()
        for j, time in enumerate(line):
            race_conditions[j,i] = line[j]
    
    moe = 1
    for race in race_conditions:
        moe *= winning(race)
        
    print(moe)
    

if __name__ == "__main__":
    main2("input")