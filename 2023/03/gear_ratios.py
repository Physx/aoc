from utils import read
import numpy as np



def get_numbers(array, signs):
    numbers = []
    new_number = ""
    status = False
    for i, line in enumerate(array):
        for j, x in enumerate(line):
            if x.isdigit():
                new_number += x
                if check_adjecent(i, j , array, signs):
                    status = check_adjecent(i, j , array, signs)
            try:
                if not array[i, j+1].isdigit():
                    if new_number:
                        numbers.append((new_number, status))
                        new_number = ""
                        status = False
            except:
                pass
    return numbers

def check_adjecent(i, j, array, signs):
    if array[i-1, j-1] in signs:
        return (i-1, j-1)
    elif array[i-1, j] in signs:
        return (i-1, j)
    elif array[i-1, j+1] in signs:
        return (i-1, j+1)
    elif array[i, j-1] in signs:
        return (i, j-1)
    elif array[i, j+1] in signs:
        return (i, j+1)
    elif array[i+1, j-1] in signs:
        return (i+1, j-1)
    elif array[i+1, j] in signs:
        return (i+1, j)
    elif array[i+1, j+1] in signs:
        return (i+1, j+1)
    else:
        return False


def main(input):
    text = read(input)

    field = np.full((len(text)+2, len(text[0])+2), fill_value=".")
    
    verify = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."]

    signs = []
    for i, line in enumerate(text, 1):
        for j, word in enumerate(line, 1):
            field[i, j] = word
            if word not in verify:
                signs.append(word)

    # part one
    # signs = list(set(signs))

    # # print(field)
    # print(signs)

    # numbers = get_numbers(field, signs)

    # summie = 0

    # for num, status in numbers:
    #     if status:
    #         summie += int(num)
    
    # print(summie)

    # part two
    signs = ["*"]
    numbers = get_numbers(field, signs)
    data = []

    for num, status in numbers:
        if status:
            data.append({status: int(num)})
    
    result_dict = {}
    partners = set()

    for item in data:
        for key, value in item.items():
            if key in result_dict:
                result_dict[key] *= value
                partners.add(key)
            else:
                result_dict[key] = value

    # Entferne Einträge ohne Partner
    result_dict = {key: value for key, value in result_dict.items() if key in partners}

    result_list = [{key: value} for key, value in result_dict.items()]

    # print(result_list)

    total_sum = sum(result_dict.values())

    print(total_sum)

if __name__ == "__main__":
    main("input")