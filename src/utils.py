def read(input):
    
    f = open(input, "r")
    
    each_line = []
    
    for line in f.readlines():
        each_line.append(line.strip())
    
    f.close()
    return each_line