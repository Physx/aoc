lines = readlines("test")
println(lines)

function calories(x)
    sum = 0
    result = []

    for line in x
        if isempty(line)
            push!(result, sum)
            sum = 0
            continue
        end
        a = parse(Int64, line)
        sum += a
    end
    return result
end


a = calories(lines)


println("Aufgabe1: ", maximum(a))

s = sort(a, rev=true)

println("Aufgabe2: ", sum(s[1:3]))
